#!/bin/sh
# @Author: Joey Teng <Toujour>
# @Date:   13-Jul-2017
# @Email:  joey.teng.dev@gmail.com
# @Filename: compile.sh
# @Last modified by:   Toujour
# @Last modified time: 14-Jul-2017
csc "/o" "/r:OptimizedPriorityQueue.4.1.1/lib/net45/Priority Queue.dll" "/out:a.exe" "/recurse:*.cs"
mono a.exe
